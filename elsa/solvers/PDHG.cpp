#include "PDHG.h"
#include "LinearOperator.h"
#include "Functional.h"
#include "TypeTraits.hpp"
#include "PowerIterations.h"
#include "elsaDefines.h"
#include "DataContainer.h"
#include "Logger.h"
#include <tuple>

namespace elsa
{
    template <class data_t>
    std::tuple<data_t, data_t> pdhg_stepsizes(std::optional<data_t> sigma,
                                              std::optional<data_t> tau, data_t Knorm)
    {
        if (!sigma.has_value() && !tau.has_value()) {
            auto sigma = 1 / Knorm;
            auto tau = 1 / Knorm;
            return {sigma, tau};
        } else if (!sigma.has_value() && tau.has_value()) {
            auto sigma = 1 / (*tau * Knorm);
            return {sigma, *tau};
        } else if (sigma.has_value() && !tau.has_value()) {
            auto tau = 1 / (*sigma * Knorm);
            return {*sigma, tau};
        } else {
            // should be unreachable, but compiler doesn't know
            return {*sigma, *tau};
        }
    }

    template <class data_t>
    PDHG<data_t>::PDHG(const LinearOperator<data_t>& K, const Functional<data_t>& f,
                       const Functional<data_t>& g, std::optional<data_t> sigma,
                       std::optional<data_t> tau, std::optional<data_t> normK,
                       SelfType_t<data_t> theta)
        : K_(K.clone()), f_(f.clone()), g_(g.clone()), theta_(theta)
    {
        if (!sigma.has_value() || !tau.has_value()) {
            auto Knorm = [&]() {
                if (normK.has_value()) {
                    return *normK;
                } else {
                    return std::sqrt(powerIterations(adjoint(K) * K));
                }
            }();

            std::tie(sigma_, tau_) = pdhg_stepsizes(sigma, tau, Knorm);
        } else {
            sigma_ = *sigma;
            tau_ = *tau;
        }
    }

    template <class data_t>
    DataContainer<data_t> PDHG<data_t>::solve(index_t iterations,
                                              std::optional<DataContainer<data_t>> x0)
    {
        auto& domain = K_->getDomainDescriptor();
        auto& range = K_->getRangeDescriptor();

        auto x = extract_or(x0, domain);
        auto x_old = x;
        auto x_relax = zeroslike(x);
        auto y = zeros<data_t>(range);

        auto primal_tmp = zeroslike(x);
        auto dual_tmp = zeroslike(y);

        Logger::get("PDHG")->info("| {:^4} | {:^12} | {:^12} |", "iter", "Primal", "Dual");

        for (index_t iter = 0; iter < iterations; ++iter) {
            // Copy required for relaxation
            x_old.assign(x);

            // Gradient asscent in dual variable
            // y_n + \sigma K(x_bar)
            K_->apply(x_relax, dual_tmp);
            lincomb(1, y, sigma_, dual_tmp, dual_tmp);

            // Apply dual proximal
            // y_{n + 1} = prox_{\sigma g^*}(y_n + \sigma K(x_bar))
            g_->proxdual(dual_tmp, sigma_, y);

            // Compute gradient in the primal
            // x_n - \sigma K^*(y_{n + 1})
            K_->applyAdjoint(y, primal_tmp);
            lincomb(1, x_old, -sigma_, primal_tmp, primal_tmp);

            // Apply primal proximal
            // y_{n + 1} = prox_{\tau f}(x_n - \sigma K^*(y_{n + 1}))
            f_->proximal(primal_tmp, tau_, x);

            // Over relaxation in the primal variable
            // x_{n+1} + \theta(x_{n+1} - x_n)
            lincomb(1 + theta_, x, -theta_, x_old, x_relax);

            auto primal = g_->evaluate(K_->apply(x)) + f_->evaluate(x);
            auto dual = -f_->convexConjugate(K_->applyAdjoint(y)) - g_->convexConjugate(y);
            auto gap = primal - dual;
            Logger::get("PDHG")->info("| {:>4} | {:12.7} | {:12.7} | {:12.7} |", iter, primal, dual,
                                      gap);
        }

        return x;
    }

    template <class data_t>
    PDHG<data_t>* PDHG<data_t>::cloneImpl() const
    {
        return new PDHG<data_t>(*K_, *f_, *g_, sigma_, tau_, false);
    }

    template <class data_t>
    bool PDHG<data_t>::isEqual(const Solver<data_t>& other) const
    {
        return false;
        auto o = downcast_safe<PDHG>(&other);
        if (!o) {
            return false;
        }

        return *K_ == *o->K_ && *f_ == *o->f_ && *g_ == *o->g_ && sigma_ == o->sigma_
               && tau_ == o->tau_ && theta_ == o->theta_;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class PDHG<float>;
    template class PDHG<double>;
} // namespace elsa
