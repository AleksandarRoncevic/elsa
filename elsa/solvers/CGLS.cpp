#include "CGLS.h"
#include "DataContainer.h"
#include "Error.h"
#include "LinearOperator.h"
#include "LinearResidual.h"
#include "TypeCasts.hpp"
#include "spdlog/fmt/bundled/core.h"
#include "spdlog/stopwatch.h"
#include "Logger.h"

namespace elsa
{
    template <class data_t>
    CGLS<data_t>::CGLS(const LinearOperator<data_t>& A, const DataContainer<data_t>& b,
                       SelfType_t<data_t> eps, SelfType_t<data_t> tol)
        : A_(A.clone()),
          b_(b),
          r_(empty<data_t>(A.getDomainDescriptor())),
          s_(empty<data_t>(A.getRangeDescriptor())),
          c_(empty<data_t>(A.getDomainDescriptor())),
          q_(empty<data_t>(A.getRangeDescriptor())),
          k_(100000000),
          kold_(100000000),
          damp_(eps * eps),
          tol_(tol)
    {
        this->name_ = "CGLS";
    }

    template <typename data_t>
    DataContainer<data_t> CGLS<data_t>::setup(std::optional<DataContainer<data_t>> x0)
    {
        auto x = extract_or(x0, A_->getDomainDescriptor());

        if (x0.has_value()) {
            x = *x0;

            // s = b_ - A_->applx(x)
            A_->apply(x, s_);
            lincomb(1, b_, -1, s_, s_);

            A_->applyAdjoint(s_, r_);
            lincomb(1, r_, -damp_, x, r_);
        } else {
            x = 0;
            s_ = b_;
            A_->applyAdjoint(s_, r_);
        }

        c_ = r_;

        k_ = r_.squaredL2Norm();
        kold_ = k_;

        // setup done!
        this->configured_ = true;

        return x;
    }

    template <typename data_t>
    DataContainer<data_t> CGLS<data_t>::step(DataContainer<data_t> x)
    {
        A_->apply(c_, q_);

        auto delta = q_.squaredL2Norm();
        auto alpha = [&]() {
            if (damp_ == 0) {
                return kold_ / delta;
            } else {
                return kold_ / (delta + damp_ * c_.squaredL2Norm());
            }
        }();

        lincomb(1, x, alpha, c_, x);
        s_ -= alpha * q_;

        A_->applyAdjoint(s_, r_);
        if (damp_ != 0.0) {
            r_ -= damp_ * x;
        }

        k_ = r_.squaredL2Norm();
        auto beta = k_ / kold_;

        // c = r + beta * c;
        lincomb(1, r_, beta, c_, c_);
        kold_ = k_;
        return x;
    }

    template <typename data_t>
    bool CGLS<data_t>::shouldStop() const
    {
        return kold_ < tol_;
    }

    template <typename data_t>
    std::string CGLS<data_t>::formatHeader() const
    {
        return fmt::format("{:^15} | {:^15}", "|| x ||_2", "|| s ||_2");
    }

    template <typename data_t>
    std::string CGLS<data_t>::formatStep(const DataContainer<data_t>& x) const
    {
        return fmt::format("{:>15.10} | {:>15.10}", x.l2Norm(), s_.l2Norm());
    }

    template <class data_t>
    CGLS<data_t>* CGLS<data_t>::cloneImpl() const
    {
        return new CGLS(*A_, b_, std::sqrt(damp_));
    }

    template <class data_t>
    bool CGLS<data_t>::isEqual(const Solver<data_t>& other) const
    {
        auto otherCGLS = downcast_safe<CGLS>(&other);

        return otherCGLS && *otherCGLS->A_ == *A_ && otherCGLS->b_ == b_
               && otherCGLS->damp_ == damp_;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class CGLS<float>;
    template class CGLS<double>;

} // namespace elsa
