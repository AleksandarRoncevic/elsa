#pragma once

#include "LinearOperator.h"
#include "BlockDescriptor.h"
#include "FiniteDifferences.h"
#include "IdenticalBlocksDescriptor.h"

#include <vector>

namespace elsa
{
    /**
     * @brief Operator to compute symmetrized derivative, represents second derivative term for
     * total generalized variation
     *
     * Domain of this operator should always contain two blocks
     *
     * Applied on a container @f$x@f$, it returns the following 3-block container:
     *
     * @f[
     * \left[\begin{array}{c}
     * D_1^{+} x_1 \\
     * D_2^{+} x_2 \\
     * \frac{1}{2}\left(D_2^{+} x^1+D_1^{+} x^2\right)
     * \end{array}\right]
     * @f]
     *
     * where @f$D_1^{+}@f$ stays for forward finite differences operator along x dimension.
     *
     * Adjoing on @f$y@f$:
     *
     * @f[
     * \left[\begin{array}{l}
     * -D_1^{-} y^1 - \D_2^{-} y^3 \\
     * -D_1^{-} y^3 - \D_2^{-} y^2
     * \end{array}\right]
     * @f]
     *
     * where @f$D_1^{-}@f$ is backwards finite differences operator along x dimension.
     *
     * References:
     * - Mei, Jin-Jin, et al. "Second order total generalized variation for speckle reduction in
     * ultrasound images." Journal of the Franklin Institute 355.1 (2018): 574-595, p.577
     *
     * - Bredies, Kristian, and Hong Peng Sun. "Preconditioned Douglas–Rachford algorithms for
     * TV-and TGV-regularized variational imaging problems." Journal of Mathematical Imaging and
     * Vision 52.3 (2015): 317-344.
     */
    template <typename data_t = real_t>
    class SymmetrizedDerivative : public LinearOperator<data_t>
    {
    public:
        SymmetrizedDerivative(const DataDescriptor& domainDescriptor);

        ~SymmetrizedDerivative() override = default;

    protected:
        /// default copy constructor, hidden from non-derived classes to prevent potential slicing
        SymmetrizedDerivative(const SymmetrizedDerivative<data_t>&) = default;

        /// apply the symmetrized derivative operator
        void applyImpl(const DataContainer<data_t>& x, DataContainer<data_t>& Ax) const override;

        /// apply the adjoint of the symmetrized derivative operator
        void applyAdjointImpl(const DataContainer<data_t>& y,
                              DataContainer<data_t>& Aty) const override;

        /// implement the polymorphic clone operation
        SymmetrizedDerivative<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LinearOperator<data_t>& other) const override;

    private:
        static LinearOperator<data_t> createBase(const DataDescriptor& domainDescriptor);
        void precomputeHelpers();

        std::unique_ptr<FiniteDifferences<data_t>> forwardX_;
        std::unique_ptr<FiniteDifferences<data_t>> forwardY_;
        std::unique_ptr<FiniteDifferences<data_t>> backwardX_;
        std::unique_ptr<FiniteDifferences<data_t>> backwardY_;
    };
} // namespace elsa
