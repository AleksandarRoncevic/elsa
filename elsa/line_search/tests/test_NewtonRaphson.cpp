#include "doctest/doctest.h"

#include "NewtonRaphson.h"
#include "DataContainer.h"
#include "VolumeDescriptor.h"
#include "LeastSquares.h"
#include "MatrixOperator.h"
#include "elsaDefines.h"

TEST_CASE_TEMPLATE("NewtonRaphson: Basic testing", data_t, float, double)
{
    elsa::Matrix_t<data_t> mat({{1, 0, 0}, {0, 1, 0}, {0, 0, 1}});
    elsa::MatrixOperator<data_t> A(mat);

    elsa::VolumeDescriptor desc({3});
    elsa::DataContainer<data_t> b(desc,
                                  elsa::Vector_t<data_t>({{data_t{1}, data_t{2}, data_t{3}}}));

    elsa::LeastSquares<data_t> ls(A, b);
    elsa::NewtonRaphson<data_t> search(ls, 1);

    elsa::DataContainer<data_t> x(desc,
                                  elsa::Vector_t<data_t>({{data_t{1}, data_t{0}, data_t{0}}}));
    auto d = -ls.getGradient(x);

    auto alpha = search.solve(x, d);

    CHECK_EQ(alpha, doctest::Approx(1));
}
