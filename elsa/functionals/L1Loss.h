#pragma once

#include "DataDescriptor.h"
#include "Functional.h"
#include "DataContainer.h"
#include "LinearOperator.h"

namespace elsa
{
    /**
     * @brief L1 loss functional modeling a laplacian noise distribution
     *
     * The least squares loss is given by:
     * \[
     * || A(x) - b ||_1
     * \]
     * i.e. the \f$\ell^1\f$ norm of the linear residual / error.
     *
     * @tparam data_t data type for the domain of the residual of the functional, defaulting to
     * real_t
     */
    template <typename data_t = real_t>
    class L1Loss : public Functional<data_t>
    {
    public:
        /**
         * @brief Constructor the l1 loss  functional
         *
         * @param[in] A LinearOperator to use in the residual
         * @param[in] b data to use in the linear residual
         */
        L1Loss(const LinearOperator<data_t>& A, const DataContainer<data_t>& b);

        /// make copy constructor deletion explicit
        L1Loss(const L1Loss<data_t>&) = delete;

        /// default destructor
        ~L1Loss() override = default;

        const LinearOperator<data_t>& getOperator() const;

        const DataContainer<data_t>& getDataVector() const;

        bool isDifferentiable() const override;

    protected:
        /// the evaluation of the l1 loss
        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        /// the computation of the gradient (in place)
        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>& out) const override;

        /// the computation of the Hessian
        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        /// implement the polymorphic clone operation
        L1Loss<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const Functional<data_t>& other) const override;

    private:
        std::unique_ptr<LinearOperator<data_t>> A_{};

        DataContainer<data_t> b_{};
    };

} // namespace elsa
