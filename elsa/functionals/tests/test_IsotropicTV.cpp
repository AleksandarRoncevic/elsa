#include <doctest/doctest.h>

#include "testHelpers.h"
#include "IsotropicTV.h"
#include "VolumeDescriptor.h"
#include "IdenticalBlocksDescriptor.h"

using namespace elsa;
using namespace doctest;

TEST_SUITE_BEGIN("functionals");

TEST_CASE_TEMPLATE("IsotropicTV", TestType, float, double, complex<float>, complex<double>)
{
    using Vector = Eigen::Matrix<TestType, Eigen::Dynamic, 1>;

    GIVEN("1 block case")
    {
        IndexVector_t numCoeff(1);
        numCoeff << 4;
        VolumeDescriptor dd(numCoeff);

        WHEN("instantiating")
        {
            IsotropicTV<TestType> func(dd);

            THEN("the functional is as expected")
            {
                REQUIRE_EQ(func.getDomainDescriptor(), dd);
            }

            THEN("a clone behaves as expected")
            {
                auto ITVClone = func.clone();

                REQUIRE_NE(ITVClone.get(), &func);
                REQUIRE_EQ(*ITVClone, func);
            }

            THEN("the evaluate, gradient and Hessian work as expected")
            {
                Vector dataVec(dd.getNumberOfCoefficients());
                dataVec << 0.1, -4.0, 5.4, -0.001;
                DataContainer<TestType> dc(dd, dataVec);

                REQUIRE(checkApproxEq(func.evaluate(dc), 18.902));
                REQUIRE_THROWS_AS(func.getGradient(dc), LogicError);
                REQUIRE_THROWS_AS(func.getHessian(dc), LogicError);
            }
        }
    }

    GIVEN("2 blocks case")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 3, 2;
        VolumeDescriptor dd(numCoeff);
        IdenticalBlocksDescriptor ibd(2, dd);

        WHEN("instantiating")
        {
            IsotropicTV<TestType> func(ibd);

            THEN("the functional is as expected")
            {
                REQUIRE_EQ(func.getDomainDescriptor(), ibd);
            }

            THEN("a clone behaves as expected")
            {
                auto ITVClone = func.clone();

                REQUIRE_NE(ITVClone.get(), &func);
                REQUIRE_EQ(*ITVClone, func);
            }

            THEN("the evaluate, gradient and Hessian work as expected")
            {
                Vector dataVec(ibd.getNumberOfCoefficients());
                dataVec << -2, -1, 6, 3, 10, -4, 6, 4, 2, -3, 5, 3;
                DataContainer<TestType> dc(ibd, dataVec);

                REQUIRE(checkApproxEq(func.evaluate(dc), 112.574));
                REQUIRE_THROWS_AS(func.getGradient(dc), LogicError);
                REQUIRE_THROWS_AS(func.getHessian(dc), LogicError);
            }
        }
    }

    GIVEN("3 blocks case")
    {
        IndexVector_t numCoeff(2);
        numCoeff << 2, 3;
        VolumeDescriptor dd(numCoeff);
        IdenticalBlocksDescriptor ibd(3, dd);

        WHEN("instantiating")
        {
            IsotropicTV<TestType> func(ibd);

            THEN("the functional is as expected")
            {
                REQUIRE_EQ(func.getDomainDescriptor(), ibd);
            }

            THEN("a clone behaves as expected")
            {
                auto ITVClone = func.clone();

                REQUIRE_NE(ITVClone.get(), &func);
                REQUIRE_EQ(*ITVClone, func);
            }

            THEN("the evaluate, gradient and Hessian work as expected")
            {
                Vector dataVec(ibd.getNumberOfCoefficients());
                dataVec << -2, -1, 6, 3, 10, -4, 6, 4, 2, -3, 5, 3, 0, -3, -6, 6, 33, 1;
                DataContainer<TestType> dc(ibd, dataVec);

                REQUIRE(checkApproxEq(func.evaluate(dc), 259.41));
                REQUIRE_THROWS_AS(func.getGradient(dc), LogicError);
                REQUIRE_THROWS_AS(func.getHessian(dc), LogicError);
            }
        }
    }
}