#pragma once

#include "DataContainer.h"
#include "DetectorDescriptor.h"

#include "StrongTypes.h"
#include "Phantoms.h"
#include "PhantomDefines.h"
#include "VolumeDescriptor.h"
#include "elsaDefines.h"
#include <Eigen/src/Core/Diagonal.h>
#include <Eigen/src/Core/DiagonalMatrix.h>
#include <Eigen/src/Core/Matrix.h>
#include <Eigen/src/Core/util/Constants.h>
#include <Eigen/src/Geometry/AngleAxis.h>
#include <Eigen/src/Geometry/Scaling.h>
#include <Eigen/src/Geometry/Transform.h>

namespace elsa::phantoms
{

    template <int n, typename data_t = real_t>
    struct QuadraticForm {
        data_t weight;
        Eigen::Vector<data_t, n> center;
        Eigen::Matrix<data_t, n, n> A;
    };

    template <int n, typename data_t>
    data_t intersectionLength(const Ray_t<data_t>& ray, const QuadraticForm<n, data_t>& ellipse);

    template <int n, typename data_t = real_t>
    class Canvas
    {
    public:
        Canvas() = default;

        void add(const QuadraticForm<n, data_t>& e) { ellipsoids.push_back(e); }

        DataContainer<data_t> makeSinogram(const DataDescriptor& sinogramDescriptor);

    private:
        std::vector<QuadraticForm<n, data_t>> ellipsoids;
    };

    template <typename data_t>
    DataContainer<data_t> analyticalSheppLogan(const VolumeDescriptor& imageDescriptor,
                                               const DetectorDescriptor& sinogramDescriptor);

} // namespace elsa::phantoms