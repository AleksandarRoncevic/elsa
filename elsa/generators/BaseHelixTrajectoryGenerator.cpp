#include "BaseHelixTrajectoryGenerator.h"
#include "Logger.h"

namespace elsa
{
    std::tuple<IndexVector_t, RealVector_t, std::vector<Geometry>>
        BaseHelixTrajectoryGenerator::createTrajectoryData(
            const DataDescriptor& volumeDescriptor, std::vector<real_t> thetas, real_t pitch,
            real_t sourceToCenter, real_t centerToDetector,
            std::optional<RealVector_t> principalPointOffset,
            std::optional<RealVector_t> centerOfRotOffset,
            std::optional<IndexVector_t> detectorSize, std::optional<RealVector_t> detectorSpacing)
    {
        // pull in geometry namespace, to reduce cluttering
        using namespace geometry;

        // sanity check
        const auto dim = volumeDescriptor.getNumberOfDimensions();

        if (dim != 3)
            throw InvalidArgumentError("HelixTrajectoryGenerator: can only handle 3d");

        Logger::get("HelixTrajectoryGenerator")
            ->info("creating {}D helical trajectory with {} poses and pitch of {}", dim,
                   thetas.size(), pitch);

        // Calculate size and spacing for each geometry pose using a IIFE
        const auto [coeffs, spacing] = calculateSizeAndSpacingPerGeometry(
            volumeDescriptor, thetas.size(), detectorSize, detectorSpacing);

        // Create vector to store geometry configurations for each pose
        std::vector<Geometry> geometryList;
        geometryList.reserve(static_cast<std::size_t>(thetas.size()));

        // Iterate over each pose and calculate its specific geometry
        for (auto degree : thetas) {
            const auto angle = Degree{degree}.to_radian();

            geometryList.emplace_back(
                SourceToCenterOfRotation{sourceToCenter},
                CenterOfRotationToDetector{centerToDetector},
                VolumeData3D{volumeDescriptor.getSpacingPerDimension(),
                             volumeDescriptor.getLocationOfOrigin()},
                SinogramData3D{Size3D{coeffs}, Spacing3D{spacing}},
                RotationAngles3D{Radian{angle}, Radian{0}, Radian{0}},
                principalPointOffset ? PrincipalPointOffset2D{principalPointOffset.value()}
                                     : PrincipalPointOffset2D{0, 0},
                centerOfRotOffset ? RotationOffset3D{centerOfRotOffset.value()[0],
                                                     pitch * angle + centerOfRotOffset.value()[1],
                                                     centerOfRotOffset.value()[2]}
                                  : RotationOffset3D{0, pitch * angle, 0});
        }

        return std::make_tuple(std::move(coeffs), std::move(spacing), std::move(geometryList));
    }

} // namespace elsa
