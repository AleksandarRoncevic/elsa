#include "elsaDefines.h"
#include <AnalyticalSinogram.h>

namespace elsa::phantoms
{

    template <int n, typename data_t>
    data_t intersectionLength(const Ray_t<data_t>& ray, const QuadraticForm<n, data_t>& ellipse)
    {
        Eigen::Vector<data_t, n> o = ray.origin() - ellipse.center;
        Eigen::Vector<data_t, n> d = ray.direction();

        auto alpha = d.dot(ellipse.A * d);
        auto beta = d.dot(ellipse.A * o);
        auto gamma = o.dot(ellipse.A * o) - 1;
        auto discriminant = beta * beta / (alpha * alpha) - gamma / alpha;

        if (discriminant >= 0) {
            return 2 * sqrt(discriminant) * ellipse.weight;
        } else {
            return 0;
        }
    }

    template <int n, typename data_t>
    DataContainer<data_t> Canvas<n, data_t>::makeSinogram(const DataDescriptor& sinogramDescriptor)
    {
        assert(is<DetectorDescriptor>(sinogramDescriptor));
        assert(sinogramDescriptor.getNumberOfDimensions() == 2
               || sinogramDescriptor.getNumberOfDimensions() == 3);

        DataContainer<data_t> sinogram{sinogramDescriptor};
        auto& detDesc = downcast<DetectorDescriptor>(sinogramDescriptor);

#pragma omp parallel for
        for (index_t index = 0; index < detDesc.getNumberOfCoefficients(); index++) {

            auto coord = detDesc.getCoordinateFromIndex(index);
            auto ray = detDesc.computeRayFromDetectorCoord(coord);
            for (auto ellipse : ellipsoids) {
                sinogram[index] += intersectionLength<n, data_t>(ray.cast<data_t>(), ellipse);
            }
        }
        return sinogram;
    }

    template <typename data_t>
    DataContainer<data_t> analyticalSheppLogan(const VolumeDescriptor& imageDescriptor,
                                               const DetectorDescriptor& sinogramDescriptor)
    {
        auto imgDim = imageDescriptor.getNumberOfDimensions();
        auto sinoDim = sinogramDescriptor.getNumberOfDimensions();
        if (!((imgDim == 2 && sinoDim == 2) || (imgDim == 3 && sinoDim == 3))) {
            throw InvalidArgumentError("only 2/3d shepplogan supported (yet)");
        }

        const auto& coeffs = imageDescriptor.getNumberOfCoefficientsPerDimension();

        if (coeffs[0] != coeffs[1]) {
            throw InvalidArgumentError("only square shepplogan supported!");
        }

        if (imgDim == 3) {
            Canvas<3, data_t> sheppLogan{};
            // TODO: Think about inversion of y-axis in 3D case.

            Eigen::Transform<data_t, 3, Eigen::Affine> scale;

            scale.linear() = coeffs[0] / 2.0
                             * imageDescriptor.getSpacingPerDimension().cast<data_t>().asDiagonal();
            scale.translation() = imageDescriptor.getLocationOfOrigin().cast<data_t>();

            for (auto [A, a, b, c, x0, y0, z0, phi, theta, psi] :
                 modifiedSheppLoganParameters<data_t>) {

                Eigen::Vector3<data_t> center{x0, y0, z0};
                Eigen::Vector3<data_t> axes{a, b, c};

                Eigen::Matrix3<data_t> rotation;
                fillRotationMatrix({phi, theta, psi}, rotation);

                center = scale * center;
                axes = scale.linear() * axes;

                Eigen::Matrix3<data_t> axMat = axes.array()
                                                   .square()
                                                   .cwiseInverse()
                                                   .matrix()
                                                   .asDiagonal(); // Principal axis
                                                                  // matrix
                                                                  // diag(a^-2,b^-2,c^-2)
                Eigen::Matrix3<data_t> rotatedQuadraticForm =
                    rotation * axMat * rotation.transpose(); // R A R^T

                sheppLogan.add(QuadraticForm<3, data_t>{A, center, rotatedQuadraticForm});
            }
            return sheppLogan.makeSinogram(sinogramDescriptor);
        } else {

            Canvas<2, data_t> sheppLogan{};
            Eigen::Transform<data_t, 2, Eigen::Affine>
                scale; // Translates coordinates from modifiedSheppLoganParameter's coordinates to
                       // image coordinates

            Eigen::Matrix2<data_t> flipY =
                Eigen::Vector2<data_t>{1, -1}.asDiagonal(); // Flip y axis as it seems to grow
                                                            // towards the bottom of the image

            scale.linear() = coeffs[0] / 2.0
                             * imageDescriptor.getSpacingPerDimension().cast<data_t>().asDiagonal()
                             * flipY;

            scale.translation() = imageDescriptor.getLocationOfOrigin().cast<data_t>();

            for (auto [A, a, b, c, x0, y0, z0, phi, theta, psi] :
                 modifiedSheppLoganParameters<data_t>) {

                Eigen::Vector2<data_t> center{x0, y0};
                Eigen::Vector2<data_t> axes{a, b};

                // Eigen::Rotation2D<data_t> rotation{geometry::Radian{geometry::Degree{phi}}};
                Eigen::Rotation2D<data_t> rotation{pi<data_t> / 180 * phi};

                center = scale * center;
                axes = scale.linear() * axes;

                Eigen::Matrix2<data_t> axMat =
                    axes.array().square().cwiseInverse().matrix().asDiagonal(); // Principal axis
                                                                                // matrix
                                                                                // diag(a^-2,b^-2)

                Eigen::Matrix2<data_t> rotatedQuadraticForm =
                    flipY * rotation * axMat * rotation.inverse() * flipY; // R A R^T

                sheppLogan.add(QuadraticForm<2, data_t>{A, center, rotatedQuadraticForm});
            }
            return sheppLogan.makeSinogram(sinogramDescriptor);
        }
    }

    template struct QuadraticForm<2, float>;
    template struct QuadraticForm<2, double>;
    template struct QuadraticForm<3, float>;
    template struct QuadraticForm<3, double>;

    template class Canvas<2, float>;
    template class Canvas<2, double>;
    template class Canvas<3, float>;
    template class Canvas<3, double>;

    template DataContainer<float> analyticalSheppLogan<float>(const VolumeDescriptor&,
                                                              const DetectorDescriptor&);
    template DataContainer<double> analyticalSheppLogan<double>(const VolumeDescriptor&,
                                                                const DetectorDescriptor&);

} // namespace elsa::phantoms