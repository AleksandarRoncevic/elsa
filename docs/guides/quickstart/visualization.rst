Visualizing Geometry and Reconstruction with pyelsa
###################################################

Quick Setup
-----------

1. **Import Libraries**

   Begin by importing ``pyelsa`` and ``numpy``:

   .. code-block:: python

      import pyelsa as elsa
      import numpy as np

2. **Create Phantom and Generate Sinogram**

   Quickly set up a Shepp-Logan phantom and simulate its CT acquisition through a circular trajectory:

   .. code-block:: python

      size = np.array([128, 128, 128])
      phantom = elsa.phantoms.modifiedSheppLogan(size)
      volume_descriptor = phantom.getDataDescriptor()

      sino_descriptor = elsa.CircleTrajectoryGenerator.createTrajectory(
          420, volume_descriptor, 180, size[0], size[0]
      )

      projector = elsa.SiddonsMethod(volume_descriptor, sino_descriptor)
      sinogram = projector.apply(phantom)

Visualization with ``visualize_reconstruction`` function
--------------------------------------------------------

**Function Overview**

The ``visualize_reconstruction`` function is designed to offer an intuitive and interactive way to visualize reconstructed CT images. 
It supports a range of customizable parameters to enhance the visualization experience:

.. code-block:: python

   elsa.visualize_reconstruction(reconstruction, dimensions_to_display=[0, 1, 2], cmap="gray")

**Customizing Visualizations**

- **Changing the Dimensions Displayed**

  To visualize specific planes, adjust the ``dimensions_to_display`` parameter. For instance, to display only the Axial and Coronal planes:

  .. code-block:: python

     elsa.visualize_reconstruction(reconstruction, dimensions_to_display=[0, 1])

- **Altering the Colormap**

  Changing the colormap can highlight different aspects of the data. For example, to use a "hot" colormap:

  .. code-block:: python

     elsa.visualize_reconstruction(reconstruction, cmap="hot")

**Interactive Features**

The function includes interactive scrolling to navigate through slices, synchronization of slices across displayed dimensions for 3D data, 
and the ability to adjust the scroll step size for slice navigation. 
These features are designed to provide a comprehensive and user-friendly way to explore the reconstructed images in detail.

Visualizing Geometry
---------------------

To complement the reconstruction visualization, ``pyelsa`` also offers geometry visualization, shedding light on the spatial arrangement of the X-ray source, detector, and object:

.. code-block:: python

   elsa.visualize_geometry(sino_descriptor, volume_descriptor)

This function features an interactive slider for exploring geometry poses, detailed visualization of crucial CT components, and X-ray source trajectory. 
This intuitive interface makes it simpler to grasp the complex spatial relationships of a CT scan.
