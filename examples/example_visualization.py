import pyelsa as elsa
import numpy as np

size = np.array([128, 128, 128])
phantom = elsa.phantoms.modifiedSheppLogan(size)
volume_descriptor = phantom.getDataDescriptor()

num_angles = 420
arc = 180

sino_descriptor = elsa.CircleTrajectoryGenerator.createTrajectory(
    num_angles, phantom.getDataDescriptor(), arc, size[0], size[0]
)

projector = elsa.SiddonsMethod(volume_descriptor, sino_descriptor)

sinogram = projector.apply(phantom)

solver = elsa.APGD(
    projector, sinogram, elsa.IndicatorNonNegativity(projector.getDomainDescriptor())
)

reconstruction = solver.solve(70)

elsa.visualize_reconstruction(reconstruction)
elsa.visualize_geometry(sino_descriptor, volume_descriptor)
