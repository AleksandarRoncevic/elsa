import argparse
from pathlib import Path
from tqdm import trange
import numpy as np
import pyelsa as elsa

from utils import setup_problem, setup


def solve(recon_types, axdt_op, dci, ffa, ffb, a, b, geos, vol_desc, range_desc, period, output=Path("."), iters=10, iters_per_step=5):
    """Simple Solve function that solves the same problem for all given reconstruction types"""
    for typeid, recon_type in enumerate(recon_types):
        print(f"Reconstruction for Noise model {recon_type}")
        fn = setup_problem(recon_type, axdt_op, dci, ffa, ffb,
                           a, b, geos, vol_desc, range_desc, period)

        x_desc = fn.getDomainDescriptor()

        linesearch = elsa.NewtonRaphson(fn, 1)
        # linesearch = elsa.BarzilaiBorwein(fn)
        solver = elsa.CGNL(fn, linesearch)

        result = elsa.DataContainer(x_desc)
        result.set(0)

        r = trange(iters // iters_per_step)

        for i in r:
            r.set_description(
                f"Model {recon_type}, Iter {i * iters_per_step} - {(i+1) *iters_per_step}")
            result = solver.run(iters_per_step, result, True)
            print(result.shape)
            np.save(
                output / f"recon_{str(typeid + 1).zfill(2)}_{recon_type}_dci_iters_{str((i + 1) * iters_per_step).zfill(2)}", np.asarray(result.getBlock(1)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='')

    parser.add_argument("--config", type=Path, required=True,
                        help="Path to 'dataset_dci.yml' file")
    parser.add_argument("--output", type=Path, default="./output",
                        help="Directory to store results in")
    parser.add_argument("--binning", "-b", type=int, default=2,
                        help="Binning factor reducing resolution but also computational cost")
    parser.add_argument("--period", "-p", type=int,
                        default=8, help="Grating periods")
    parser.add_argument("--recon-type", nargs="+", type=str,
                        default=["gd", "glogd", "gb", "rb"], help="Noise model: Gaussian log(d), Gaussian d, Rician b (approximated by Gaussian), and Rician b")
    parser.add_argument("--iters", type=int, default=10, help="Number of iterations for reconstruction")
    parser.add_argument("--iters-per-step", type = int,
                        default = 5, help = "Number of iterations per step")

    args=parser.parse_args()

    config_path=args.config
    output_folder=args.output
    binning_factor=args.binning
    period=args.period
    iters=args.iters
    iters_per_step=args.iters_per_step

    recon_types=args.recon_type
    if recon_types[0] == "all":
        recon_types=["gd", "glogd", "gb", "rb"]

    axdt_op, dci, ffa, ffb, a, b, geos, vol_desc, range_desc=setup(
        config_path, binning_factor)

    solve(recon_types, axdt_op, dci, ffa, ffb, a, b, geos, vol_desc,
          range_desc, period, output_folder, iters, iters_per_step)
